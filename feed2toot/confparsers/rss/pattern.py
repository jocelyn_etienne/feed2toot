# -*- coding: utf-8 -*-
# Copyright © 2015-2021 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get value of the patterne option of rss section
'''Get value of the pattern option of the rss section'''

# standard library imports
import logging
import sys

def splitsubs(s):
    splitted=s.split(s[0])
    if len(splitted)!=3 and ( len(splitted)!=4 or splitted[3] not in ['','g'] ): 
        raise Exception("not a substitution pattern `"+s+"'");
    splitted+=['']
    return splitted[1:4]

def parsepattern(config):
    '''Parse configuration value of the pattern option of the rss section'''
    patterns = {}
    patternscasesensitive = {}
    patternsregex = {}
    strip = {}
    stringsep = ','
    section = 'rss'
    if config.has_section(section):
        #######################
        # pattern format option
        #######################
        for pattern in ['summary_detail', 'published_parsed', 'guidislink', 'authors', 'links', 'title_detail', 'author', 'author_detail', 'comments', 'published', 'summary', 'tags', 'title', 'link', 'id']:
            currentoption = '{}_pattern'.format(pattern)
            if config.has_option(section, currentoption):
                tmppattern = config.get(section, currentoption)
                if stringsep in tmppattern:
                    patterns[currentoption] = [i for i in tmppattern.split(stringsep) if i]
                else:
                    patterns[currentoption] = [tmppattern]

            ###############################
            # pattern_case_sensitive option
            ###############################
            currentoption = '{}_pattern_case_sensitive'.format(pattern)
            if config.has_option(section, currentoption):
                try:
                    patternscasesensitive[currentoption] = config.getboolean(section, currentoption)
                except ValueError as err:
                    logging.warn(err)
                    patternscasesensitive[currentoption] = True
            else:
                # default value
                patternscasesensitive[currentoption] = False

            ######################
            # pattern_regex option
            ######################
            currentoption = '{}_pattern_regex'.format(pattern)
            if config.has_option(section, currentoption):
                try:
                    patternsregex[currentoption] = config.getboolean(section, currentoption)
                except ValueError as err:
                    logging.warn(err)
                    patternsregex[currentoption] = True
            else:
                # default value
                patternsregex[currentoption] = False
            ############
            # sub option
            ############
            currentoption = '{}_sub'.format(pattern)
            if config.has_option(section, currentoption):
                tmppattern = config.get(section, currentoption)
                try:
                  # different patterns are separated by 'stringsep' when it follows an 'end-of-subst'
                  # character, else the 'stringsep' is within pattern
                  if tmppattern[0]+stringsep in tmppattern:
                    strip[currentoption] = [splitsubs(i) for i in tmppattern.split(tmppattern[0]+stringsep) if i]
                  else:
                    strip[currentoption] = [splitsubs(tmppattern)]
                except Exception as err:
                    logging.error("option {} error: {}".format(currentoption, err))
                    exit(1)
    return patterns, patternscasesensitive, patternsregex, strip
