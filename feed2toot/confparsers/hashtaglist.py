# -*- coding: utf-8 -*-
# Copyright © 2015-2021 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get values of the hashtaglist section
'''Get values of the hashtaglist section'''

# standard library imports
import os.path
import sys
import codecs

def extracthashtagsfromlist(detecthashtaglist, prepend, append):
    '''extract hashtags from the the list'''
    keywords = codecs.open(detecthashtaglist, encoding='utf-8').readlines()
    # in each: leader=i.split('|')[0]
    keywords = [ [i.split('|')[0],prepend+i.rstrip()+append]
            for i in keywords if i[0] not in ['#',';']]
    return keywords

def parsehashtaglist(clioption, config):
    '''Parse configuration values and get values of the hashtaglist section'''
    hashtaglist = ''
    detecthashtaglist = ''
    detectkeywords = []
    section = 'hashtaglist'
    if not clioption:
        ####################################
        # several_words_hashtags_list option
        ####################################
        if config.has_section(section):
            confoption = 'several_words_hashtags_list'
            if config.has_option(section, confoption):
                hashtaglist = config.get(section, confoption)
                hashtaglist = os.path.expanduser(hashtaglist)
                if not os.path.exists(hashtaglist) or not os.path.isfile(hashtaglist):
                    sys.exit('The path to the several_words_hashtags_list parameter is not valid: {hashtaglist}'.format(hashtaglist=hashtaglist))
            confoption = 'detect_keywords'
            if config.has_option(section, confoption):
                detecthashtaglist = config.get(section, confoption)
                detecthashtaglist = os.path.expanduser(detecthashtaglist)
                if not os.path.exists(detecthashtaglist) or not os.path.isfile(detecthashtaglist):
                    sys.exit('The path to the detect_keywords parameter is not valid: {detecthashtaglist}'.format(detecthashtaglist=detecthashtaglist))
                # match whole words, case insensitive
                prepend=r'\b(?i:'
                append=r')\b'
                if config.has_option(section, 'detect_keywords_regex'):
                    if config.get(section, 'detect_keywords_regex'):
                       prepend=''
                       append=''
                detectkeywords=extracthashtagsfromlist(detecthashtaglist, prepend, append)
    return hashtaglist, detectkeywords
