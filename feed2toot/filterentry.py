# -*- coding: utf-8 -*-
# Copyright © 2015-2021 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Filter an entry of the RSS feeds
'''Filter an entry of the RSS feeds'''

# standard library imports
import sys
import re
import logging
import feedparser

class FilterEntry:
    '''FilterEntry class'''
    def __init__(self, elements, entry, options, byrsspatterns, rssobject, feedname):
        '''Constructor of the FilterEntry class'''
        self.matching = {}
        self.entry = entry
        self.elements = elements
        self.options = options
        self.byrsspatterns = byrsspatterns
        self.rssobject = rssobject
        self.feedname = feedname
        self.main()

    def main(self):
        '''Main of the FilterEntry class'''
        authorized_elements = ['feedname', ]
        authorized_elements.extend(self.entry.keys())
        for i in self.elements:
            self.detectkeywords(i)
            self.strip(i)
            if i not in authorized_elements:
                sys.exit('The element {} is not available in the RSS feed. The available ones are: {}'.format(i, [j for j in self.entry]))
            # for the case if no pattern at all is defined
            if i == 'feedname':
                self.matching[i] = self.feedname
            elif not self.options['patterns'] and not self.byrsspatterns and not self.rssobject:
                self.matching[i] = self.entry[i]
            # global filter only
            elif self.options['patterns'] and not self.byrsspatterns and not self.rssobject:
                if not self.options['nopatternurinoglobalpattern']:
                    self.applyglobalfilter(i)
                else:
                    self.matching[i] = self.entry[i]
            # global filter and then by rss filter
            elif self.options['patterns'] and self.byrsspatterns and self.rssobject:
                # patterns by rss
                self.applyglobalfilter(i)
                self.applyspecificfilter(i)
            elif not self.options['patterns'] and self.byrsspatterns and self.rssobject:
                self.applyspecificfilter(i)
            else:
                self.matching[i] = self.entry[i]

    def strip(self, i):
        '''Remove text according to a regex'''
        for patternlist in self.options['strip']:
            if '{}_sub'.format(i)==patternlist:
                for pattern in self.options['strip'][patternlist]:
                  if pattern[2]=='g':
                    count=0
                  else:
                    count=1
                  #print("  Sub: ",pattern[0]," with ",pattern[1])
                  if type(self.entry[i])==list:
                    if type(self.entry[i][0])==feedparser.FeedParserDict:
                        try:
                            self.entry[i]=", ".join(
                                [", ".join([eii for eii in ei.values()]) for ei in self.entry[i]])
                        except Exception as Err:
                            logging.warn("Error handling field '{}' : {}".format(i,Err))

                  self.entry[i]=re.sub(pattern[0],pattern[1],self.entry[i],count=count)

    def detectkeywords(self, i):
        '''Add a hashtag when entry matches the pattern'''
        for pattern in self.options['detectkeywords']:
                    regex=re.compile(pattern[1])
                    match=re.search(regex, self.entry['title']+" "+self.entry['summary'])
                    #print("get "+pattern.ljust(20)[:20],end='')
                    if match:
                        #print(" YES")
                        if i==self.elements[0]:
                            if 'tags' not in self.entry:
                                #self.entry['tags']=[{'term':match.group(0)}]
                                self.entry['tags']=[{'term':pattern[0]}]
                            else:
                                #self.entry['tags']+=[{'term':match.group(0)}]
                                self.entry['tags']+=[{'term':pattern[0]}]

    def applyglobalfilter(self, i):
        '''Apply the global filter'''
        for patternlist in self.options['patterns']:
            #if patternlist=='{}_pattern'.format(i) and self.options['patternsregex']['{}_regex'.format(patternlist)]:
            if self.options['patternsregex']['{}_regex'.format(patternlist)]:
                #print("? "+self.feedname+" matches "+i+" pattern plist "+patternlist)
                matchthispattern=True
                for pattern in self.options['patterns'][patternlist]:
                    # regex patterns need to be *all* found to be matching
                    regex=re.compile(pattern)
                    match=re.search(regex, self.entry[patternlist.split('_')[0]])
                    if match:
                        if i==self.elements[0]:
                            if 'tags' not in self.entry:
                                self.entry['tags']=[{'term':match.group(0)}]
                            else:
                                self.entry['tags']+=[{'term':match.group(0)}]
                    else: matchthispattern=False
                # but if all found in one element of entry, then it is fully validated:
                if matchthispattern:
                    #print("- "+self.feedname+" matches "+i+" pattern "+pattern+" plist "+patternlist)
                    for j in self.entry: self.matching[j] = self.entry[j]
            elif not self.options['patternscasesensitive']['{}_case_sensitive'.format(patternlist)]:
                # not case sensitive, so we compare the lower case
                for pattern in self.options['patterns'][patternlist]:
                    finalpattern = pattern.lower()
                    finaltitle = self.entry[patternlist.split('_')[0]].lower()
                    if finalpattern in finaltitle:
                        self.matching[i] = self.entry[i]
            else:
                # case sensitive, so we use the user-defined pattern
                for pattern in self.options['patterns'][patternlist]:
                    if pattern in self.entry[patternlist.split('_')[0]]:
                        self.matching[i] = self.entry[i]

    def applyspecificfilter(self, i):
        '''Apply specific filters for by-rss pattern matching'''
        for byrsspattern in self.byrsspatterns:
            byrssfinalpattern = byrsspattern.lower()
            if byrssfinalpattern in self.entry[self.rssobject].lower():
                self.matching[i] = self.entry[i]

    @property
    def finalentry(self):
        '''Return the processed entry'''
        return self.matching
